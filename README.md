# doi-package Test
PHP library for calling the DOI micro service


## Getting started

Make sure to run the DOI endpoint, then install the library by:

Composer install
```
composer require aria-php/doiprovider
```

In your code environment (.env), you need to assign the docker name to `ARIA_DOI_DOCAKER_NAME` and the port number to `ARIA_DOI_PORT_NUMBER` 

## Test and Deploy
The folder `tests` includes a few tests
## Authors and acknowledgment
Omran Alhaddad - omran.alhaddad@instruct-eric.org
