<?php

namespace ARIA\DOIProvider;

/**
 *
 * @author Omran Alhaddad <omran.alhaddad@instruct-eric.org>
 */
class DOIProvider
{
    private string $resolver = 'doi_api';
    private int $port = 80;

    public function __construct(string $host = 'doi_api', int $port = 80)
    {
        $this->resolver = $host;
        $this->port = $port;
    }

    /**
     * @description                     -  Calling the DOI micro service
     * @param  String $filterType       - the type can be either 'doi', 'title', or 'author'
     * @param  String $filterValue      - the assigned value
     * @return Array  Retuens an array that includes 'data' - the response; 'error' - 0 or 1; and/or 'code' - the 
     */
    public function doiFilter($filterType, $filterValue)
    {
        $response = array();

        $filterValue = trim($filterValue);

        if($filterType == 'doi' || $filterType == 'title' || $filterType == 'author'){
            if(!empty($filterValue)){
                $url = 'http://'. $this->resolver .'/api/v1/search?filter['.$filterType.']='.urlencode($filterValue).'';
                $port = $this->port;
                $response = self::HTTPGet($url, $port);
            }else{
                $response['data']  = "The DOI filter doesn't include a value";
                $response['error'] = 1;
            }
        }else{
            $response['data']  = "The DOI filter doesn't include a filter type";
            $response['error'] = 1;
        }
        return $response;
    }

    /**
     * @description - Make HTTP-GET call
     * @param       $url - the HTTP GET for the required URL
     * @param       array $port - the port number
     * @return      HTTP-Response body or an empty string if the request fails or is empty
     */
    protected function HTTPGet($url, $port) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_HTTPGET, true); 
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_FAILONERROR, true);
        
        if($port)
            curl_setopt($ch, CURLOPT_PORT, $port);
        $resp = curl_exec($ch);
        $resp = array();
        $resp['data'] = curl_exec($ch);
        $resp['code'] = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $resp['error'] = 0;
        
        if (!$resp['data']) {
            $resp['error'] = 1;
            $resp['data'] = 'Error code:'.$resp['code'].", Error: ".curl_error($ch).", Attempted query: ".$url;
            $resp['code'] = $resp['code'];
        }
        curl_close($ch);
        
        return $resp;
    }

   
}