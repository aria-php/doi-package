<?php

namespace ARIA\DOIProviderTest;

use PHPUnit\Framework\TestCase;
use ARIA\DOIProvider\DOIProvider;

/**
 *
 * @author Omran Alhaddad <omran.alhaddad@instruct-eric.org>
 */
class DOIProviderTest extends TestCase
{
    public function testdoiFilterTestFilterType() {
        $DOIProviderClass = new DOIProvider($_ENV['ARIA_DOI_DOCAKER_NAME'], $_ENV['ARIA_DOI_PORT_NUMBER']);
        $res = $DOIProviderClass->doiFilter("name", "test"); // 'name' is doesn't exists, so I expected an error
        $this->assertEquals(1,$res['error']);

        $res = $DOIProviderClass->doiFilter("title", "test"); // 'title' is existing, so I expected no error
        $this->assertEquals(0,$res['error']); // replace $class1 with $this
    }

    public function testdoiFilterTestFilterValue() {
        $DOIProviderClass = new DOIProvider();
        $res = $DOIProviderClass->doiFilter("title", ""); // Empty value, so I expected an error
        $this->assertEquals(1,$res['error']);
    }
}
